using System;
using System.Collections.Generic;
using System.Linq;
using ContainerVervoer.Classes;
using Xunit;

namespace ContainerVervoerTests
{
    public class ContainerStackTests
    {
        [Fact]
        public void InstantiatingAContainerStack_WithDefaults_CreatesAWorkingContainerStack()
        {
            //Arrange
            var stack = new ContainerStack();
            
            //Act
            
            //Assert
            Assert.False(stack.Cooled);
        }

        [Fact]
        public void InstantiatingAContainerStack_WithAndWithoutRefridgeratedParameter_CreatesAContainerStackWithOrWithoutRefridgerated()
        {
            //Arrange
            var stack1 = new ContainerStack(false, true);
            var stack2 = new ContainerStack(false, false);
            
            //Act
            
            //Assert
            Assert.True(stack1.Cooled);
            Assert.False(stack2.Cooled);
        }

        [Fact]
        public void AddContainer_CooledToNonCooledStack_ReturnsFalse()
        {
            //Arrange
            var container = new Container(4500, false, true);
            var stack = new ContainerStack(false, false);
            //Act
            var containerAdded = stack.AddContainer(container);

            //Assert
            Assert.False(containerAdded);
        }

        [Fact]
        public void AddContainer_CooledToCooledStack_ReturnsTrue()
        {
            //Arrange
            var container = new Container(4500, false, true);
            var stack = new ContainerStack(false, true);
            //Act
            var containerAdded = stack.AddContainer(container);

            //Assert
            Assert.True(containerAdded);
        }

        [Fact]
        public void AddContainer_ToStackWithValuableOnTop_ReturnsFalse()
        {
            //Arrange
            var container = new Container(4500, true, false);
            var container1 = new Container(4500, false, false);
            var containers = new List<Container> {container};
            var stack = new ContainerStack(containers);
            //Act
            var containerAdded = stack.AddContainer(container1);

            //Assert
            Assert.False(containerAdded);
        }
        
        [Fact]
        public void AddContainer_ToStackWithNonValuable_ReturnsTrue()
        {
            //Arrange
            var container = new Container(4500, false, false);
            var container1 = new Container(4500, false, false);
            var containers = new List<Container> {container};
            var stack = new ContainerStack(containers);
            
            //Act
            var containerAdded = stack.AddContainer(container1);

            //Assert
            Assert.True(containerAdded);
        }
        
        [Fact]
        public void AddContainer_WithTooMuchWeightToStack_ReturnsFalse()
        {
            //Arrange
            var container = new Container(30000, false, false);
            container.AddTopWeight(120000);
            var container1 = new Container(30000, false, false);
            var stack = new ContainerStack();
            
            //Act
            stack.AddContainer(container);
            stack.AddContainer(container1);
            var containerAdded = stack.AddContainer(container1);

            //Assert
            Assert.False(containerAdded);
        }

        [Fact]
        public void GettingTheTotalWeight_AfterAddingACoupleOfContainers_ReturnsTheRightWeight()
        {
            //Arrange
            var container = new Container(5000, false, false);
            var container1 = new Container(5000, false, false);
            var container2 = new Container(10000, false, false);
            var stack = new ContainerStack();
            
            //Act
            stack.AddContainer(container);
            stack.AddContainer(container1);
            stack.AddContainer(container2);

            //Assert
            Assert.Equal(20000, stack.TotalWeight());
        }
    }
}