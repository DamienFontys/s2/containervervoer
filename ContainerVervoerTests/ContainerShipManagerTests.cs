using System.Collections.Generic;
using ContainerVervoer.Classes;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Xunit;

namespace ContainerVervoerTests
{
    public class ContainerShipManagerTests
    {
        [Fact]
        public void Ship_OnANewShipManager_ReturnsAnInstanceWithAShip()
        {
            //Arrange
            var containerShipManager = new ContainerShipManager(1,1);
            
            //Act
           
            
            //Assert
            Assert.NotNull(containerShipManager.Ship);
        }

        [Fact]
        public void ShipManager_OnCalculateWithNoInput_ReturnsAnEmptyShip()
        {
            //Arrange
            var containerShipManager = new ContainerShipManager(1, 1);
            var input = new List<Container>();
            
            //Act
            var output = containerShipManager.Calculate(input);

            //Assert
            Assert.Empty(output.ContainerGrid[0, 0].Containers);
        }

        [Fact]
        public void ShipManager_OnCalculateWithOneInput_ReturnsAShipWithTheRightPositioning()
        {
            //Arrange
            var containerShipManager = new ContainerShipManager(1,1);
            var input = new List<Container> {new Container(5000, false, false)};

            //Act
            var output = containerShipManager.Calculate(input);

            //Assert
            Assert.NotEmpty(output.ContainerGrid[0, 0].Containers);
        }

        [Fact]
        public void ShipManager_OnCalculateWithTwoInputs_ReturnsAShipWithTheRightPositioning()
        {
            //Arrange
            var containerShipManager = new ContainerShipManager(1,1);
            var input = new List<Container> {new Container(5000, false, false), new Container(5000, true, false)};

            //Act
            var output = containerShipManager.Calculate(input);

            //Assert
            Assert.NotEmpty(output.ContainerGrid[0, 0].Containers);
            Assert.Empty(containerShipManager.RemainingContainers);
        }
    }
}