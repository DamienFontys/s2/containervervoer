using ContainerVervoer.Classes;
using Xunit;

namespace ContainerVervoerTests
{
    public class ShipTests
    {
        [Fact]
        public void AddingAContainerToShip_WhenItsTheFirstContainer_ReturnsTrue()
        {
            //Arrange
            var container = new Container(4000, false, false);
            var ship = new Ship(3,3);
            //Act
            var added = ship.AddContainer(container);

            //Assert
            Assert.True(added);
        }

        [Fact]
        public void AddingAContainerToShip_WhileFirstContainerIsValuable_Returns_False()
        {
            //Arrange
            var container = new Container(4000, true, false);
            var container1 = new Container(4000, false, false);
            var ship = new Ship(1,1);

            //Act
            ship.AddContainer(container);
            var added = ship.AddContainer(container1);

            //Assert
            Assert.False(added);
        }

        [Fact]
        public void AddingAContainer_ThatWillDestabilizeBalance_GetPutsOnTheOtherSide()
        {
            //Arrange
            var container = new Container(4000, false, false);
            var container2 = new Container(4000, false, false);
            
            var ship = new Ship(2,2);
            ship.AddContainer(container);
            
            //Act
            ship.AddContainer(container2);
            
            var grid = ship.ContainerGrid;
            var count1 = grid[0, 0].Containers.Count;
            var count2 = grid[1, 0].Containers.Count;
            //Assert
            Assert.Equal(count1, count2);

        }
    }
}