using ContainerVervoer.Classes;
using Xunit;

namespace ContainerVervoerTests
{
    public class ContainerTests
    {
        private Container ContainerNonValuableCooled { get; set; }
        
        public ContainerTests()
        {
            ContainerNonValuableCooled = new Container(5000, false, true);
        }
        
        [Fact]
        public void InstantiatingAContainer_WithValuableTrueWithoutCooledTrue_ReturnsAValuableNonCooledContainer()
        {
            //Arrange
            var container = new Container(5000, true, false);

            //Act
            var valuable = container.Valuable;
            var cooled = container.Cooled;

            //Assert
            Assert.True(valuable);
            Assert.False(cooled);
        }

        [Fact]
        public void InstantiatingAContainer_WithValuableTrueWithCooledTrue_ReturnsAValuableCooledContainer()
        {
            //Arrange
            var container = new Container(5000, true, true);
            
            //Act
            var valuable = container.Valuable;
            var cooled = container.Cooled;

            //Assert
            Assert.True(valuable);
            Assert.True(cooled);
        }

        [Fact]
        public void InstantiatingAContainer_WithValuableFalseWithCooledTrue_ReturnsANonValuableCooledContainer()
        {
            //Arrange
            var container = new Container(5000, false, true);
            
            //Act
            var valuable = container.Valuable;
            var cooled = container.Cooled;

            //Assert
            Assert.False(valuable);
            Assert.True(cooled);
        }

        [Fact]
        public void InstantiatingAContainer_WithValuableFalseWithCooledFalse_ReturnsANonValuableNonCooledContainer()
        {
            //Arrange
            var container = new Container(5000, false, false);
            
            //Act
            var valuable = container.Valuable;
            var cooled = container.Cooled;
            
            //Assert
            Assert.False(valuable);
            Assert.False(cooled);
        }

        [Fact]
        public void Weight_OfTheContainer_ReturnsWeight()
        {
            //Arrange
            var container = new Container(5000, false, false);
            
            //Act
            var weight = container.Weight;

            //Assert
            Assert.Equal(5000, weight);
        }

        [Fact]
        public void WeightOnTop_OfTheContainer_ReturnsWeightOfContainersOnTop()
        {
            //Arrange
            var container = new Container(5000, false, false);
            var container2 = new Container(5000, false, false);
            var container3 = new Container(5000, false, false);
            var containerStack = new ContainerStack();
            
            //Act
            containerStack.AddContainer(container);
            containerStack.AddContainer(container2);
            containerStack.AddContainer(container3);

            //Assert
            Assert.Equal(10000, container.WeightOnTop);
        }
    }
}