using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;

namespace ContainerVervoer.Classes
{
    public class ContainerShipManager
    {
        public Ship Ship { get; private set; }
        
        public List<Container> RemainingContainers { get; private set; } = new List<Container>();

        public ContainerShipManager(int width, int length)
        {
            Ship = new Ship(width, length);
        }
        
        public Ship Calculate(IEnumerable<Container> containersInput)
        {
            //Order containers to have non-cooled containers last
            var orderedContainersInput = containersInput.OrderBy(c => c.Cooled).ToList();
            
            //Order containers to have non-valuable containers last
            orderedContainersInput = orderedContainersInput.OrderBy(c => c.Valuable).ToList();
            
            //Loop through all the containers
            foreach (var container in orderedContainersInput)
            {
                //Add container to remaining containers
                RemainingContainers.Add(container);
                if (Ship.AddContainer(container))
                {
                    //If container could be added, then remove container from remainingcontainers again
                    RemainingContainers.Remove(container);
                }
            }
            
            return Ship;
        }
    }
}