using System.Collections.Generic;
using System.Linq;

namespace ContainerVervoer.Classes
{
    public class ContainerStack
    {
        public List<Container> Containers { get; }
        public bool Cooled { get; }
        public bool FirstRow { get; }
        public bool LastRow { get; }

        public ContainerStack()
        {
            Containers = new List<Container>();
            Cooled = false;
            FirstRow = false;
            LastRow = false;
        }
        
        public ContainerStack(bool firstRow, bool lastRow)
        {
            Containers = new List<Container>();
            Cooled = lastRow;
            FirstRow = firstRow;
            LastRow = lastRow;
        }

        public ContainerStack(List<Container> containers)
        {
            Containers = containers;
        }
        
        public bool AddContainer(Container container)
        {
            //Check if container is allowed
            if (!ContainerIsAllowed(container))
            {
                return false;
            }


            //Foreach container in the stack
            foreach (var existingContainer in Containers)
            {
                //Add the weight of the container to the combined weight of the stack
                existingContainer.AddTopWeight(container.Weight);
            }
            
            Containers.Add(container);                
            return true;
        }

        private bool ContainerIsAllowed(Container container)
        {
            //Return false when the stack is not refridgerated while the container needs cooling
            if (!Cooled && container.Cooled)
            {
                return false;
            }

            //Return false if the stack is not the first or last row and the container is valuable
            if (!FirstRow && !LastRow && container.Valuable)
            {
                return false;
            }

            //Return false if the top container is valuable
            if (TopContainerIsValuable())
            {
                return false;
            }

            //Return false if the weight of the current container is not allowed on the current stack
            if (!WeightAllowed(container))
            {
                return false;
            }
            
            return true;
        }
        
        //Return the top container's valuable state
        private bool TopContainerIsValuable()
        {
            return Containers.Count > 0 && Containers.Last().Valuable;
        }

        //Check if adding the weight of the container is allowed on this stack
        private bool WeightAllowed(Container container)
        {
            if (Containers.Count == 0)
            {
                return true;
            }

            return Containers.First().WeightOnTop + container.Weight < 120000;
        }

        //Get the total weight of the stack
        public int TotalWeight()
        {
            var totalWeight = 0;

            foreach (var container in Containers)
            {
                totalWeight += container.Weight;
            }
            
            return totalWeight;
        }
    }
}