using System;
using System.Collections.Generic;

namespace ContainerVervoer.Classes
{
    public class Ship
    {
        public int Width { get; }
        private int Length { get; }

        private int LastRow { get; }
        public ContainerStack[,] ContainerGrid { get; private set; }
        
        public Ship(int width, int length)
        {
            Width = width;
            Length = length;
            LastRow = length - 1;
            ContainerGrid = new ContainerStack[width, length];
            for (var w = 0; w < width; w++)
            {
                for (var l = 0; l < length; l++)
                {
                    if (l == 0)
                    {
                        //Make the containerStack know that its the first row
                        ContainerGrid[w, l] = new ContainerStack(true, false);
                    }
                    else if (l == length - 1)
                    {
                        //Make the containerStack know that its the last row
                        ContainerGrid[w, l] = new ContainerStack(false, true);                         
                    }
                    else
                    {
                        //Make the containerStack know that its in one of the middle rows
                        ContainerGrid[w, l] = new ContainerStack(false, false);
                    }
                }
            }
        }

        public bool AddContainer(Container container)
        {
            //Foreach column on the ship
            for (var w = 0; w < Width; w++)
            {
                //Check if the ship holds its balance if the container gets added in the current stack
                if (!AddingContainerToColumnKeepsBalance(container, w)) continue;
                
                
                //Foreach row in teh current colimn on the ship
                for (var l = 0; l < Length; l++)
                {
                    //Check if the container can be added on containerGrid niveau
                    if (ContainerGrid[w, l].AddContainer(container))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //Gets the weight for multiple columns
        private int GetWeightForColumns(int startColumn, int endColumn)
        {
            var weight = 0;
            
            //Loop through all the specified columns and add the weight of every stack
            for (var i = startColumn; i < endColumn; i++)
            {
                for (var l = 0; l < Length; l++)
                {
                    weight += ContainerGrid[i, l].TotalWeight();
                }
            }
            return weight;
        }
        
        //Check if adding a container to the current column keeps the balance on the ship
        private bool AddingContainerToColumnKeepsBalance(Container container, int column)
        {   
            int leftWeight;
            int rightWeight;
            
            var width = ContainerGrid.GetLength(0);
            var mid = width / 2;

            var shipIsUneven = width % 2 != 0;

            if (column == mid)
            {
                return true;
            }
            
            if (shipIsUneven)
            {
                //uneven
                leftWeight = GetWeightForColumns(0, mid + 1);
                rightWeight = GetWeightForColumns(mid + 1, width);
            }
            else
            {
                //even
                leftWeight = GetWeightForColumns(0, mid);
                rightWeight = GetWeightForColumns(mid, width);
            }

            if (shipIsUneven)
            {
                if (column < mid + 1)
                {
                    leftWeight += container.Weight;
                }
                else if(column > mid + 1)
                {
                    rightWeight += container.Weight;
                }
            }
            else
            {
                if (column < mid)
                {
                    leftWeight += container.Weight;
                }
                else
                {
                    rightWeight += container.Weight;
                }
            }

            var totalWeight = leftWeight + rightWeight;

            int leftPercent;
            int rightPercent;
            
            if (totalWeight != 0)
            {
                leftPercent = leftWeight / totalWeight * 100;
                rightPercent = rightWeight / totalWeight * 100;
            }
            else
            {

                leftPercent = 0;
                rightPercent = 0;
            }


            int percentDiff;
            if (leftPercent < rightPercent)
            {
                percentDiff = rightPercent - leftPercent;
            }
            else
            {
                percentDiff = leftPercent - rightPercent;
            }

            if (percentDiff > 20 && percentDiff <= 100)
            {
                return false;
            }
            
            return true;
        }

    }
}