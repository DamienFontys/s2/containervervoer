namespace ContainerVervoer.Classes
{
    public class Container
    {

        public int Weight { get; }
        public bool Valuable { get; }
        public bool Cooled { get; }
        public int WeightOnTop { get; private set; }
        
        public Container(int weight, bool valuable, bool cooled)
        {
            Weight = weight;
            Valuable = valuable;
            Cooled = cooled;
            WeightOnTop = 0;
        }

        public void AddTopWeight(int weight)
        {
            WeightOnTop += weight;
        }
        
    }
}