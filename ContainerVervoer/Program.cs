﻿using System;
using System.Collections.Generic;
using ContainerVervoer.Classes;

namespace ContainerVervoer
{
    class Program
    {
        static void Main(string[] args)
        {   
            var csm = new ContainerShipManager(5, 10);

            var inputContainers = new List<Container>
            {
                new Container(4000, false, false),
                new Container(8000, false, false),
                new Container(4000, true, true),
                new Container(6400, false, true),
                new Container(4000, false, false),
                new Container(5600, true, false),
                new Container(6000, false, false),
                new Container(4000, false, true),
                new Container(25000, true, true),
                new Container(4000, false, false),
                new Container(11630, false, false),
                new Container(4000, false, true),
                new Container(3250, true, false),
                new Container(20000, false, false),
                new Container(4000, false, false),
                new Container(8000, false, false),
                new Container(4000, true, true),
                new Container(6400, false, true),
                new Container(4000, false, false),
                new Container(5600, true, false),
                new Container(6000, false, false),
                new Container(4000, false, true),
                new Container(25000, true, true),
                new Container(4000, false, false),
                new Container(11630, false, false),
                new Container(4000, false, true),
                new Container(3250, true, false),
                new Container(20000, false, false),
                new Container(4000, false, false),
                new Container(8000, false, false),
                new Container(4000, true, true),
                new Container(6400, false, true),
                new Container(4000, false, false),
                new Container(5600, true, false),
                new Container(6000, false, false),
                new Container(4000, false, true),
                new Container(25000, true, true),
                new Container(4000, false, false),
                new Container(11630, false, false),
                new Container(4000, false, true),
                new Container(3250, true, false),
                new Container(20000, false, false),
                new Container(4000, false, false),
                new Container(8000, false, false),
                new Container(4000, true, true),
                new Container(6400, false, true),
                new Container(4000, false, false),
                new Container(5600, true, false),
                new Container(6000, false, false),
                new Container(4000, false, true),
                new Container(25000, true, true),
                new Container(4000, false, false),
                new Container(11630, false, false),
                new Container(4000, false, true),
                new Container(3250, true, false),
                new Container(20000, false, false),
                new Container(4000, false, false),
                new Container(8000, false, false),
                new Container(4000, true, true),
                new Container(6400, false, true),
                new Container(4000, false, false),
                new Container(5600, true, false),
                new Container(6000, false, false),
                new Container(4000, false, true),
                new Container(25000, true, true),
                new Container(4000, false, false),
                new Container(11630, false, false),
                new Container(4000, false, true),
                new Container(3250, true, false),
                new Container(20000, false, false)
            };
            
            var result = csm.Calculate(inputContainers);
            var output = csm.RemainingContainers;

        }
    }
}